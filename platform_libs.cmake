if (CMAKE_SYSTEM MATCHES "Linux*")
    # Linux Specific flags
    set (OPENSSL_SSL         ${CMAKE_CURRENT_LIST_DIR}/openssl/1.1/lin64/libssl.a)
    set (OPENSSL_CRYPTO      ${CMAKE_CURRENT_LIST_DIR}/openssl/1.1/lin64/libcrypto.a)
    set (OPENSSL_INCLUDE     ${CMAKE_CURRENT_LIST_DIR}/openssl/1.1/include)

    # opencore-amr libraries
    set (OPENCORE_AMRNB      ${CMAKE_CURRENT_LIST_DIR}/opencore-amr/linux/libopencore-amrnb.a)
    set (OPENCORE_AMRWB      ${CMAKE_CURRENT_LIST_DIR}/opencore-amr/linux/libopencore-amrwb.a)

    # uuid
    set (UUID_LIB            ${CMAKE_CURRENT_LIST_DIR}/uuid/lib/lin64/libuuid.a)
    set (UUID_INCLUDE        ${CMAKE_CURRENT_LIST_DIR}/uuid/include)

    # event2
    set (EVENT2_INCLUDE      ${CMAKE_CURRENT_LIST_DIR}/event2/include)
    set (EVENT2_LIB          ${CMAKE_CURRENT_LIST_DIR}/event2/linux/x64/libevent.a)
    set (EVENT2_LIB_CORE     ${CMAKE_CURRENT_LIST_DIR}/event2/linux/x64/libevent_core.a)
    set (EVENT2_LIB_EXTRA    ${CMAKE_CURRENT_LIST_DIR}/event2/linux/x64/libevent_extra.a)
    set (EVENT2_LIB_PTHREADS ${CMAKE_CURRENT_LIST_DIR}/event2/linux/x64/libevent_pthreads.a)
    set (EVENT2_LIB_OPENSSL  ${CMAKE_CURRENT_LIST_DIR}/event2/linux/x64/libevent_openssl.a)

    # Opus
    set (OPUS_INCLUDE        ${CMAKE_CURRENT_LIST_DIR}/opus/include)
    if (RASPBERRY)
        set (OPUS_LIB        ${CMAKE_CURRENT_LIST_DIR}/opus/rpi/libopus.a)
    else()
        set (OPUS_LIB        ${CMAKE_CURRENT_LIST_DIR}/opus/linux/libopus.a)
    endif()

    # Portaudio
    set (PORTAUDIO_LIB       ${CMAKE_CURRENT_LIST_DIR}/portaudio/linux/x64/libportaudio.a)
    set (PORTAUDIO_INCLUDE   ${CMAKE_CURRENT_LIST_DIR}/portaudio/include)

    set (PCAP_LIB            ${CMAKE_CURRENT_LIST_DIR}/pcap/linux/x64/libpcap.a)
    set (PCAP_INCLUDE        ${CMAKE_CURRENT_LIST_DIR}/pcap/include)
endif()

if (ANDROID_ABI)
    # Linux Specific flags
    set (OPENSSL_SSL         ${CMAKE_CURRENT_LIST_DIR}/openssl/1.1/android/${ANDROID_ABI}/libssl.a)
    set (OPENSSL_CRYPTO      ${CMAKE_CURRENT_LIST_DIR}/openssl/1.1/android/${ANDROID_ABI}/libcrypto.a)
    set (OPENSSL_INCLUDE     ${CMAKE_CURRENT_LIST_DIR}/openssl/1.1/include)
endif()

if (CMAKE_SYSTEM MATCHES "Windows*")
    # Sndfile
    set (SNDFILE_INCLUDE     ${CMAKE_CURRENT_LIST_DIR}/sndfile/include)
    set (SNDFILE_LIBS        ${CMAKE_CURRENT_LIST_DIR}/sndfile/win64/static/FLAC.lib
                             ${CMAKE_CURRENT_LIST_DIR}/sndfile/win64/static/flac++.lib
                             ${CMAKE_CURRENT_LIST_DIR}/sndfile/win64/static/sndfile.lib
                             ${CMAKE_CURRENT_LIST_DIR}/sndfile/win64/static/ogg.lib
                             ${CMAKE_CURRENT_LIST_DIR}/sndfile/win64/static/vorbis.lib
                             ${CMAKE_CURRENT_LIST_DIR}/sndfile/win64/static/vorbisfile.lib
                             ${CMAKE_CURRENT_LIST_DIR}/sndfile/win64/static/vorbisenc.lib
                             ${CMAKE_CURRENT_LIST_DIR}/sndfile/win64/static/opus.lib
                             )

    # Opus libraries
    set (OPUS_INCLUDE        ${CMAKE_CURRENT_LIST_DIR}/opus/include)
    set (OPUS_LIB            ${CMAKE_CURRENT_LIST_DIR}/opus/win64/release/opus.lib)

    # Opencore AMR libraries
    set (OPENCORE_AMRNB      ${CMAKE_CURRENT_LIST_DIR}/opencore-amr/win64/opencore-amrnb.lib)
    set (OPENCORE_AMRWB      ${CMAKE_CURRENT_LIST_DIR}/opencore-amr/win64/opencore-amrwb.lib)

    set (CRASHRPT_ROOT       ${CMAKE_CURRENT_LIST_DIR}/crashrpt/win64)
    set (CRASHRPT_INCLUDE    ${CMAKE_CURRENT_LIST_DIR}/crashrpt/win64/include)
    set (CRASHRPT_LIB        ${CMAKE_CURRENT_LIST_DIR}/crashrpt/win64/lib/dll/crashrpt1500.lib)

    set (PCAPPP_INCLUDE      ${CMAKE_CURRENT_LIST_DIR}/pcappp/include)

    if (CMAKE_BUILD_TYPE MATCHES "Release" OR CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo")
        set (PCAPPP_LIBS         ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Release/Pcap++.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Release/Packet++.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Release/Packet.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Release/Common++.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Release/wpcap.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Release/pthreadVC3.lib)
    else()
        set (PCAPPP_LIBS         ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Debug/Pcap++.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Debug/Packet++.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Debug/Packet.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Debug/Common++.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Debug/wpcap.lib
                                 ${CMAKE_CURRENT_LIST_DIR}/pcappp/lib/win/x64/Debug/pthreadVC3d.lib)
    endif()
endif()


if (CMAKE_SYSTEM MATCHES "Darwin*")
    set (OPENSSL_SSL         ${CMAKE_CURRENT_LIST_DIR}/openssl/1.0/osx/libssl.a)
    set (OPENSSL_CRYPTO      ${CMAKE_CURRENT_LIST_DIR}/openssl/1.0/osx/libcrypto.a)
    set (OPENSSL_INCLUDE     ${CMAKE_CURRENT_LIST_DIR}/openssl/1.0/include)

    set (SNDFILE_INCLUDE     ${CMAKE_CURRENT_LIST_DIR}/sndfile/include)
    set (SNDFILE_LIBS        ${CMAKE_CURRENT_LIST_DIR}/sndfile/macos/libsndfile.a)

    # Opus
    set (OPUS_INCLUDE        ${CMAKE_CURRENT_LIST_DIR}/opus/include)
    set (OPUS_LIB            ${CMAKE_CURRENT_LIST_DIR}/opus/osx/libopus.a)

    # Opencore AMR libraries
    set (OPENCORE_AMRNB      ${CMAKE_CURRENT_LIST_DIR}/opencore-amr/macos/libopencore-amrnb.a)
    set (OPENCORE_AMRWB      ${CMAKE_CURRENT_LIST_DIR}/opencore-amr/macos/libopencore-amrwb.a)

    # event2
    set (EVENT2_INCLUDE      ${CMAKE_CURRENT_LIST_DIR}/event2/include)
    set (EVENT2_LIB          ${CMAKE_CURRENT_LIST_DIR}/event2/macos/libevent.a)
    set (EVENT2_LIB_CORE     ${CMAKE_CURRENT_LIST_DIR}/event2/macos/libevent_core.a)
    set (EVENT2_LIB_EXTRA    ${CMAKE_CURRENT_LIST_DIR}/event2/macos/libevent_extra.a)
    set (EVENT2_LIB_PTHREADS ${CMAKE_CURRENT_LIST_DIR}/event2/macos/libevent_pthreads.a)
    set (EVENT2_LIB_OPENSSL  ${CMAKE_CURRENT_LIST_DIR}/event2/macos/libevent_openssl.a)

endif()

set (EVENT2_LIBS ${EVENT2_LIB} ${EVENT2_LIB_CORE} ${EVENT2_LIB_EXTRA} ${EVENT2_LIB_PTHREADS})
